\name{semiKmeans}
\alias{semiKmeans}
\title{
Semi-supervised K-Means Clustering
}
\description{
Perform semi-supervised k-means clustering on data matrix.
}
\usage{
semiKmeans(x, k, ml, cl, iter.max = 10)
}
\arguments{
  \item{x}{
    A numeric matrix of data, or an object that can be coerced to
    such a matrix (such as a numeric vector or a data frame with
    numeric columns only). Data is clustered row-wise.
  }
  \item{k}{
    The number of clusters. A random set of k (distinct)
    rows in \code{x} is chosen as the initial centres.
  }
  \item{ml}{
    A list of must-link tupels. Each tuple is a vector giving the
    indices of the pair of points connected with a must-link constraint.
  }
  \item{cl}{
    A list of must-link tupels. Each tuple is a vector giving the
    indices of the pair of points connected with a must-link constraint.
  }
  \item{iter.max}{
    The maximum number of iterations allowed.
  }
}
\details{
  The data given by \code{x} is clustered by the k-means method, which
  aims to partition the points into \code{k} groups such that the sum of
  squares from points to the assigned cluster centres is minimized.
  If any must-link or cannot-link constraints are given, semiKmeans will
  not violate these constraints when assigning points to nearest clusters.
}
\value{
  An object of class \code{kmeans} which is a list with components:
  \item{cluster }{A vector of integers indicating the cluster to which each point is allocated.}
  \item{centers }{A matrix of cluster centres.}
  \item{withinss }{The within-cluster sum of squares for each cluster.}
  \item{size}{The number of points in each cluster.}
  There is a \code{print} method for this class.
}
%%\references{
%% ~put references to the literature/web site here ~
%%}
\author{
  Johann M. Kraus
}
\seealso{
  \code{\link{semiBisect}}, \code{\link{semiDiana}}, \code{\link{kmeans}}
}
\examples{
x <- matrix(runif(10), nc=2)
kmeans(x, 2)
semiKmeans(x, 2, cl=list(c(1,3)))
}
\keyword{ cluster }

