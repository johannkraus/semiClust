# library(cluster)
# source("constraintUtils.R")
# source("semiKmeans.R")

################################################################################
# Description: Semi-supervised divisive hierarchical bisecting k-means
#              clustering. On each level a k-means into 2 groups is computed.
#              Additionally, a list of pairwise cannot-link and must-link
#              constraints can be included as background knowledge. See
#              'semiKmeans' for details.
# Arguments:   x: data matrix or data frame.
#              ml: list of must-link pairs. Set to 0 if omitted.
#              cl: list of cannot-link pairs. Set to 0 if omitted.
#              ...: parameters for function 'dist'.
# Value:       an object of class 'diana' representing the clustering; this
#              class has methods for the following generic functions: 'print',
#              'summary', 'plot'.
#              Further, the class 'diana' inherits from 'twins'. Therefore, the
#              generic function 'pltree' can be used on a 'diana' object, and an
#              'as.hclust' method is available. See 'diana' for more details.
# Author:      Johann M. Kraus
# Last change: 18.01.10
################################################################################
semiBisect <- function(x, ml, cl, iter.max=10, nstart=1, keep.data=F){
  if(missing(x))
    stop("'x' must be a matrix or data.frame.")
  
  # convert x to matrix
  x <- data.matrix(x)
  x.rownames <- rownames(x)
  rownames(x) <- NULL
  
  # number of samples
  N = nrow(x)
  # D = ncol(x)

  # distance matrix for getGroupToSplit
  diss <- as.matrix(dist(x))
  
  # merge matrix for class diana
  splitmat <- matrix(nr = N-1, nc = 2)
  mergemat <- matrix(nr = N-1, nc = 2)

  # height in dendrogram for class diana
  heights <- c()
  
  # divisive coefficient for class diana
  dc <- c()
  # for dc calculation, see below

  # order for class diana
  order <- list(1:N)

  initdiam <- getGroupToSplit(order, diss)$height
 
  for(i in (N-1):1){
    
    # which group to split
    grouptosplit <- getGroupToSplit(order, diss)

    # resolve constraints on first level
    if(i==(N-1)){
      # init new groups
      if(missing(ml))
        if(missing(cl))
          newgroups <- bisectGroup_constrained(x, iter.max=iter.max, nstart=nstart)
        else
          newgroups <- bisectGroup_constrained(x, cl=cl, iter.max=iter.max, nstart=nstart)
      else
        if(missing(cl))
          newgroups <- bisectGroup_constrained(x, ml=ml, iter.max=iter.max, nstart=nstart)
        else
          newgroups <- bisectGroup_constrained(x, ml=ml, cl=cl, iter.max=iter.max, nstart=nstart)
    }
    else{
      # init new groups
      newgroups <- bisectGroup(order[[grouptosplit$idx]], x, iter.max=iter.max, nstart=nstart)
    }

    # update order
    order <- order[-grouptosplit$idx]
    order <- append(order, newgroups, grouptosplit$idx-1)

    # update height
    heights <- append(heights, grouptosplit$height, grouptosplit$idx-1)

    # update merge matrix (4 cases)
    if(length(newgroups[[1]]) > 1 & length(newgroups[[2]]) > 1){
      # case 1
      splitmat[i,] <- c(grouptosplit$idx, grouptosplit$idx+1)
      mergemat[i,] <- c(grouptosplit$idx, grouptosplit$idx+1)

      if(i<N-1){
        target <- which(splitmat[(i+1):(N-1),] == grouptosplit$idx)[1]
        mergemat[(i+1):(N-1),][target] <- i
        for(j in (i+1):(N-1)){
          if(splitmat[j,1]>grouptosplit$idx)
            splitmat[j,1] <- splitmat[j,1]+1
          if(splitmat[j,2]>grouptosplit$idx)
            splitmat[j,2] <- splitmat[j,2]+1
        }
      }
      
    }
    else{
      if(length(newgroups[[1]]) == 1 & length(newgroups[[2]]) == 1){
        # case 2
        splitmat[i,] <- c(grouptosplit$idx, grouptosplit$idx+1)
        mergemat[i,] <- c((-1)*newgroups[[1]], (-1)*newgroups[[2]])

        if(i<N-1){
          target <- which(splitmat[(i+1):(N-1),] == grouptosplit$idx)[1]
          mergemat[(i+1):(N-1),][target] <- i
          for(j in (i+1):(N-1)){
            if(splitmat[j,1]>grouptosplit$idx)
              splitmat[j,1] <- splitmat[j,1]+1
            if(splitmat[j,2]>grouptosplit$idx)
              splitmat[j,2] <- splitmat[j,2]+1
          }
        }

        # update divisive coefficient
        dc <- c(dc, 1 - grouptosplit$height/initdiam, 1 - grouptosplit$height/initdiam)
      }
      else{
        if(length(newgroups[[1]]) == 1){
          # case 3
          splitmat[i,] <- c(grouptosplit$idx, grouptosplit$idx+1)
          mergemat[i,] <- c((-1)*newgroups[[1]], grouptosplit$idx+1)

          if(i<N-1){
            target <- which(splitmat[(i+1):(N-1),] == grouptosplit$idx)[1]
            mergemat[(i+1):(N-1),][target] <- i

            for(j in (i+1):(N-1)){
              if(splitmat[j,1]>grouptosplit$idx)
                splitmat[j,1] <- splitmat[j,1]+1
              if(splitmat[j,2]>grouptosplit$idx)
                splitmat[j,2] <- splitmat[j,2]+1
            }
          }

          # update divisive coefficient
          dc <- c(dc, 1 - grouptosplit$height/initdiam)
        }
        else{
          # case 4
          splitmat[i,] <- c(grouptosplit$idx, grouptosplit$idx+1)
          mergemat[i,] <- c(grouptosplit$idx, (-1)*newgroups[[2]])

          if(i<N-1){
            target <- which(splitmat[(i+1):(N-1),] == grouptosplit$idx)[1]
            mergemat[(i+1):(N-1),][target] <- i

            for(j in (i+1):(N-1)){
              if(splitmat[j,1]>grouptosplit$idx)
                splitmat[j,1] <- splitmat[j,1]+1
              if(splitmat[j,2]>grouptosplit$idx)
                splitmat[j,2] <- splitmat[j,2]+1
            }
          }

          # update divisive coefficient
          dc <- c(dc, 1 - grouptosplit$height/initdiam)
        }
      }
    }
  }

  # update divisive coefficient
  dc <- mean(dc)

  # update order
  order <- unlist(order)

  # combine results
  res <- list(order = order, height = heights, dc = dc, merge = mergemat, call = match.call())

  # order.lab for class diana
  if(!is.null(x.rownames)){
    res$order.lab <- x.rownames[order]
  }
  
  # add data if keep.data
  if(keep.data)
    res <- c(res, list(data=x))

  # output to class diana
  class(res) <- c("diana","twins")
  res
}

# group to split is the one with largest diameter
getGroupToSplit <- function(groups, diss){
  # incl. catch warning because of max(dist(0))
  # and set single dist to -1
  diam <- sapply(groups, function(u) {di <- (diss[u, u,drop=F]); if(length(di)==1){-1}else{max(di)}})
  list(idx = which.max(diam), height = max(diam))
}

# init and fill new group
bisectGroup <- function(group, x, iter.max, nstart){
  if(nrow(x[group,])>2){
    kres <- kmeans(x[group,], 2, iter.max=iter.max, nstart=nstart)
    one <- group[kres$cluster==1]
    two <- group[kres$cluster==2]
  }
  else{
    one <- group[1]
    two <- group[2]
  }
  # order newgroups to match class diana
  newgroups <- c(list(one),list(two))[order(c(one[1],two[1]))]
  newgroups
}

# init and fill new group in case of constraints (currently only on first level)
bisectGroup_constrained <- function(x, ml, cl, iter.max, nstart){
  group <- 1:nrow(x)
  kres <- semiKmeans(x, 2, ml, cl, iter.max=iter.max, nstart=nstart)
  one <- group[kres$cluster==1]
  two <- group[kres$cluster==2]
  # order newgroups to match class diana
  newgroups <- c(list(one),list(two))[order(c(one[1],two[1]))]
  newgroups
}
